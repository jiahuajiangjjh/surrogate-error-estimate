#TODO list

## 3/22/2017 [done]

- check if the code for non-orthogonal bases is correct [done]: it is correct.

- problem setting: 1D diffusion problem with 1000 samples(parameter domain)

- convergence trend:

    - true error
    - error estimate
    - surrogate error estimate

- function value for true error, error estimate and surrogate error estimate with fixed Nbbasis 
